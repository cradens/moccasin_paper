# README #

Supplemental Info for:

MOCCASIN: A method for correcting for known and unknown confounders in RNA splicing analysis

See: Moccasin Supplementary Text for details about the contents of these folders

See MoccasinTestingDirectory/run_moccasin.sh for example usage of MOCCASIN.
