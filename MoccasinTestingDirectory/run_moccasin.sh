# download MOCCASIN repository
git clone git@bitbucket.org:biociphers/moccasin.git

# set up a python environment 
python -m venv moccasin_env
source moccasin_env/bin/activate
pip install --upgrade pip
pip install -r ./moccasin/dependencies.txt


# run moccasin with 8 threads. (adjust # of threads accordingly...)
# this should take no more than a few minutes
python ./moccasin/run_moccasin.py factors.tsv ./before_moccasin/ ./after_moccasin/ batch_2 -J 8

# explore amount of variance in first 16 RUV-style discovered factors
python ./moccasin/run_moccasin.py factors.tsv ./before_moccasin/ ./after_moccasin/ batch_2 -J 8 --explore_residual_factors -U 16
